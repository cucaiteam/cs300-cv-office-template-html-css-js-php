-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 28, 2015 at 05:00 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_300`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `ID` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Content` text COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `User` varchar(40) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `ItemId` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`ID`, `Content`, `User`, `ItemId`) VALUES
('2015-12-27 14:59:37', 'very awesome, 4 stars for this one <3', 'giabao@gmail.com', '2015-12-27 14:11:27');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `ID` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Descript` text COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `File` text COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `Image` text COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `Vote` double NOT NULL DEFAULT '0',
  `NumVote` int(11) NOT NULL DEFAULT '0',
  `NumDown` int(11) NOT NULL DEFAULT '0',
  `Uploader` varchar(40) COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `Published` tinyint(1) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL,
  `Price` double NOT NULL,
  `Tag` text COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `AdImage` text COLLATE utf8mb4_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`ID`, `Name`, `Descript`, `File`, `Image`, `Vote`, `NumVote`, `NumDown`, `Uploader`, `Published`, `Type`, `Price`, `Tag`, `AdImage`) VALUES
('2015-12-27 06:10:18', 'Resume Template White Blue Title', 'This is a FREE resume template that is intended for your personal use. Please do not sell or distribute it.The file format is PSD and it uses Roboto. Open sans and Lato will work nicely as an alternative font choice.', '../images/upload/files/1451196619.zip', '../images/upload/pics/1451196619.PNG', 0, 0, 0, 'dtest@gmail.com', 0, 1, 0, 'blue, white, resume, template', ''),
('2015-12-27 14:11:27', 'Black and White Polite Template', 'Download this resume template for free. This template was designed by Fernando BÃ¡ez. Note: The file is in Adobe Illustrator (.ai) format. The download also includes a PSD file containing a set icons.', '../images/upload/files/1451225488.zip', '../images/upload/pics/1451225488.jpg', 4, 1, 0, 'giabao@gmail.com', 0, 1, 0, 'black, white, polite', '../images/upload/pics/1451225488_1.jpg;../images/upload/pics/1451225488_2.jpg;../images/upload/pics/1451225488_3.jpg'),
('2015-12-28 03:11:21', 'Strategic Planning PPT Template', 'This Strategic Planning PPT has a green background and a marketing plan with a chart. The template is suitable for marketing implementation as well as direct marketing presentations to be used by a marketing company or marketing firm. Also can be used to embed sales strategy plans into a swot presentation or PPT analysis. The template has a guy asking himself what strategy to follow.', '../images/upload/files/1451272282.zip', '../images/upload/pics/1451272282.jpg', 0, 0, 0, 'dtest@gmail.com', 0, 2, 0, 'Strategic, planning, ppt, template', '../images/upload/pics/1451272282_1.jpg;../images/upload/pics/1451272282_2.jpg;../images/upload/pics/1451272282_3.jpg'),
('2015-12-28 03:20:07', 'Technical Report Documentation ', 'This report â€œtemplateâ€ or starter document was created with most of the styles you will need built into it. These styles should appear in the Styles bar (or â€œQuick Style Galleryâ€) at the top right of your Home tab when you open this Word starter document. You might want to peruse what is there for your use in reports now. Note which style is applied to the various parts of this starter document. The style will appear as highlighted in the Styles bar on the Home tab when you click on a different part of this document.', '../images/upload/files/1451272807.docx', '../images/upload/pics/1451272807.PNG', 0, 0, 0, 'dtest@gmail.com', 0, 3, 0, 'technical, report, documentation', '../images/upload/pics/1451272807_1.PNG;../images/upload/pics/1451272807_2.PNG;../images/upload/pics/1451272807_3.PNG'),
('2015-12-28 03:35:58', 'GANYMEDE PRESENTATION TEMPLATE', 'Donâ€™t be fooled by its dark cover, Ganymede is the most colorful free presentation template at SlidesCarnival. With some surprising layouts and very big titles it has a really modern look and feel that will fit almost any topic. Your startup elevator pitch will leave a mark with this design, keep the colorful rainbow palette for the backgrounds, or choose a single color for all slides that matches your company brand.\r\n\r\nTHIS FREE PRESENTATION TEMPLATE FEATURES:\r\n\r\nFully editable. Easy to change colors, text and photos\r\n25 different slides\r\nColorful and modern design that works with any accent color\r\nGraphs, icons, tables and maps\r\nDownload this presentation as a PowerPoint PPT file and edit on your computer. Also export to PDF, JPG, etc.\r\n16:9 screen layout (Can change to 4:3 with a click on Google Slides, but some graphic assets may not work well)', '../images/upload/files/1451273759.pptx', '../images/upload/pics/1451273759.PNG', 0, 0, 0, 'dtest@gmail.com', 0, 2, 0, 'garymede, presentation, template', '../images/upload/pics/1451273759_1.PNG;../images/upload/pics/1451273759_2.PNG;../images/upload/pics/1451273759_3.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `itemtype`
--

CREATE TABLE `itemtype` (
  `ID` int(11) NOT NULL,
  `TypeName` text COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `Extension` text COLLATE utf8mb4_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `itemtype`
--

INSERT INTO `itemtype` (`ID`, `TypeName`, `Extension`) VALUES
(1, 'CV', 'ai,doc,docx,psd,zip,rar,dotx'),
(2, 'Presentation', 'pptx,potx,ppsx,rar,zip'),
(3, 'Report', 'dotx,docx,doc,rar,zip');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `Email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL DEFAULT '',
  `Password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Age` int(11) NOT NULL DEFAULT '0',
  `Sex` tinyint(1) NOT NULL DEFAULT '0',
  `CardNumber` bigint(20) DEFAULT NULL,
  `CVV` varchar(5) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `CardDate` varchar(8) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `CardType` varchar(3) COLLATE utf8_vietnamese_ci DEFAULT 'non',
  `Budget` double NOT NULL DEFAULT '0',
  `Nationality` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Role` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Name`, `Email`, `Password`, `Age`, `Sex`, `CardNumber`, `CVV`, `CardDate`, `CardType`, `Budget`, `Nationality`, `Role`) VALUES
('Äá»‹nh Duy TÃ¹ng', 'ddtung@apcs.vn', '2124214121', 0, 0, 0, NULL, '', '0', 0, '', 0),
('dtest123214', 'dtest@gmail.com', '$1$Wj..nh/.$Rin7IJoh//ArCNVgWlrJ40', 0, 0, 542152151251, '213', '02/2016', 'mas', 0, 'vietnamese', 0),
('Gia-Bao Huynh', 'giabao@gmail.com', '$2a$09$anexamplestringforsale0E42iCeQi2.NIGWJUfdyvdbAZtN.3B6', 0, 0, NULL, NULL, NULL, 'non', 0, NULL, 0),
('', 'group5', '123456', 0, 0, 0, NULL, '', '0', 0, '', 0),
('admin@group5.com', 'group5_cs300@gmail.com', '$1$em0.P0..$lmTHNqDEshuektdr9oA3H.', 20, 0, 51521421421421, '222', '02/2014', 'mas', 0, 'English', 0),
('Cao Kháº¯c LÃª Duy', 'group5@group5.com', '123456789', 20, 1, 4214215354, '124', '02/2015', 'non', 2.99, 'vietnamese', 1),
('VHoang251', 'hoangvo@gmail.com', '$1$0p1.H34.$j3ZcBHI5z7w6VKMHa6P35.', 0, 0, 0, NULL, '', '0', 0, '', 0),
('LÃª Duy Máº­t', 'ldmat@acsp.com', '$2y$11$qWa5wbR8mL1dLt7Obbf9PeZ/MfF4S2xQv0f1daNTLxt', 0, 0, 0, NULL, '', '0', 0, '', 0),
('LÃª VÄƒn Duyá»‡t', 'lvdy@apcs.vn', '$1$pZ3.8o4.$DkcrF4nOSfnG5wIWXFS/N/', 0, 0, NULL, NULL, '', NULL, 0, NULL, 0),
('VÃµ Anh HoÃ ng', 'vahoang@apcs.vn', '217501285', 0, 0, 0, NULL, '', '0', 0, '', 0),
('VÃµ Anh HoÃ ng 2', 'vahoang@gmail.com', '$1$uP..fG3.$yu/ONKiq4hb327L/dUhED.', 0, 0, 0, NULL, '', '0', 0, '', 0),
('VÃµ Anh HoÃ ng 3', 'vahoang3@gmail.com', '$1$6Q3.Vu0.$u6/F9xQkM7kJ0iR5Cc3/a0', 0, 0, 0, NULL, '', '0', 0, '', 0),
('VÃµ Anh HoÃ ng 3', 'vahoang4@gmail.com', '$1$.92.tj/.$AfTH6AFzK/18j.WMP8Wsk1', 0, 0, 0, NULL, '', '0', 0, '', 0),
('VÃµ Anh HoÃ ng 5', 'vhoang5@gmail.com', '$1$iK..Dx/.$AQp/DfXoZOxzA1RbfPRaZ.', 0, 0, 0, NULL, '', '0', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `votings`
--

CREATE TABLE `votings` (
  `ItemID` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `User` varchar(40) COLLATE utf8mb4_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `votings`
--

INSERT INTO `votings` (`ItemID`, `User`) VALUES
('2015-12-27 14:11:27', 'giabao@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `itemtype`
--
ALTER TABLE `itemtype`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Email`);

--
-- Indexes for table `votings`
--
ALTER TABLE `votings`
  ADD PRIMARY KEY (`ItemID`,`User`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
