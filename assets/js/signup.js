var userName = $("#username");
var email = $("#email");
var pwd = $("#pwd");
var agreement = $("#agreement");
var human = $("#human");
var validateUser = $("#user-validation");
var validateEmail = $("#email-validation");
var validatePwd =  $("#password-validation");
var validateOtherFields = $("#fields-validation");
$(document).keypress(function(e) {
    if(e.which == 13) {
        signup();
    }
});
document.getElementById("btnsignup").addEventListener("click",signup);

jQuery(function($){
    //Views
    //Data From views
    userName.keyup(function(){
        var name = userName.val();
        var validate = validateUser;
        console.log(name.length);
        if(name.length === 0){
          validate.text("*");
        }
        else if (name.length < 8){
          validate.text("< 8 characters");
        }
        else if(name.length > 8){
           if(checkUserName(name)===false){
            validate.text("Must contains only alphabet and numbers");
            }else{

            validate.text("Accepted");
        }
    }
    });

    email.keyup(function(){
        var mail = $("#email").val();
        var validate = validateEmail;
        if(mail.length === 0){
          validate.text("*");
        }
        else if (mail.length < 8){
          validate.text("< 8 characters");
        }
        else if(mail.length > 8){
          if(checkEmail(mail) === false ){
            validate.text("Invalid Email");
          }else{
            validate.text("Checking");
            isUserExisted(mail,checkUserExistCallback);
          }
        }

    });


    pwd.keyup(function(){
        var pwd = $("#pwd").val();
        var validate = validatePwd;
        if(pwd.length === 0){
          validate.text("*");
        }
        else if (pwd.length < 6){
          validate.text("< 6 characters");
        }
        else if(pwd.length > 6){
          if(checkPassword(pwd)===false){
            validate.text("Too weak password");
            }else{
                validate.text("Accepted");
            }
        }
    });
});


// document.getElementById("btnsignup").addEventListener("click",signup());
//Functions




function checkUserExistCallback(yes) {
    validate = validateEmail;
    if(yes == 0)
        validate.text("Accepted");
    else
        validate.text("Email existed");
}

function checkEmail(email){
      var emailRE = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if(emailRE.test(email)){
        return true;
      }
      else{
        return false;
      }
    }

function checkPassword(pwd){
    var passRE = /^[a-zA-Z0-9_-\s]{5,15}$/;
    if(passRE.test(pwd)){
      return true;
    }
    else{
      return false;
    }
}

function checkUserName(name){
  // var nameRE = /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]gi/;
  var nameRE = /^[A-Za-z\u0000-\u007F\u0111\s]/gi;
  // var nameRE = /^[A-Za-z0-9_-\s]{4,15}$/;
  if(nameRE.test(name)){
    return true;
  }
  else{
    return false;
  }
}

function signup(){
    var validateEmail = document.getElementById("email-validation").innerHTML;
    var validateUsername = document.getElementById("user-validation").innerHTML;
    var validatePwd = document.getElementById("password-validation").innerHTML;
    //Data From views
    var validateFields = [validateEmail,validateUsername,validatePwd];


    if (!isAllFilled()){
      validateOtherFields.text("You miss some fields");
      return;
    }
    if (!isAllValid(validateFields)) {
        console.log("Not valid");
        validateOtherFields.text("There are some fields not invalid");
        return;

    }

    if( !isAgreedTerm() || !isRobot() ){
          console.log("Agree and robot");
          validateOtherFields.text("You have to confirm our terms and the robot checker");
          return;
        }

    var username = userName.val();
        mail = email.val();
        password = pwd.val();
    console.log(username);
  	$.ajax({
                    type: "POST",
                    url: 'php/data_insert.php',
                    data: {
                    	'function': "addUser",
                    	'name': username,
                    	'email' : mail,
                    	'pwd' : password
                    },
                    success: function(data)
                    {
                      console.log(data);
                       if (data == true)
                            alert("success! X:" + data);
                        else
                            alert("Failed");
                    }
        });
}


function isAllValid(array){
  for (x in array) {
    if (array[x] != "Accepted"){
      return false;
    }
  }
  return true;
}

function isAllFilled(){
  return userName.val().length > 0 && pwd.val().length > 0 && email.val().length > 0;
}

function isAgreedTerm(){
  return agreement.is(':checked');
}

function isRobot(){
  return human.is(':checked');
}

function isUserExisted(mail,callBack){
    console.log(mail);
    $.ajax({
                    type: "POST",
                    url: 'php/data_get.php',
                    data: {
                        'function': "isExistUser",
                        'email' : mail
                    },
                    success: function(isExist)
                    {
                        console.log(isExist);
                        callBack(isExist);
                    }

        });
}
