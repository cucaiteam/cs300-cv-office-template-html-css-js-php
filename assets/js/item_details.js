var item = null;
var comments = null;

$("#downloadclick").click(Download);
$('#commentbtn').click(Comment);
$('#shareclick').click(EventShare);


var id = GetParamValue();
checklogin(HideLeaveComment);
if (id.length>0)
{
	GetItem(id);
	GetComments(id);
}
function GetParamValue() {
	var query = window.location.search.substring(1);
    var parms = query.split('&');
    console.log(parms);
    for (var i=0; i <parms.length; i++) 
    {
        var pos = parms[i].indexOf('=');
        if (pos > 0) {
        var key = parms[i].substring(0,pos);
        var val = parms[i].substring(pos+1);
        console.log(val);
       	return val;
       }
   }
   return ''
}

function GetComments(id){
	$.ajax({
		type : 'POST',
		url:'php/data_get.php',
		data:{
			'function' : 'getComments',
			'id' : id
		},
		success: ParsingCmt
	});
}

function GetItem(id){
	$.ajax({
		type: 'POST',
		url: 'php/data_get.php',
		data:{
			'function': 'getItem',
			'id' : id
		},
		success: Parsing
	});
}

function Parsing(response){
	console.log(response);
	item = JSON.parse(response); 
	DisplayDetails();
}

function ParsingCmt(response){
	console.log(response);
	if(response === 0) return;
	comments = JSON.parse(response);
	DisplayComments();
	
}

function DisplayDetails(){
	DisplayTitle();
	DisplayRate();
	DisplayCate();
	DisplayImage();
	DisplayDescript();
	DisplayAdImage();
}

var defaultAdimg = "images/no_image.jpg";
function DisplayAdImage(){
	var adArr = item.AdImage.split(";");
	if (item.AdImage.length == 0 ){
		for (var i = 0 ; i < 3; i++){
			$('#adimages').append('<div class="4u 12u"><img id="itemavatar1"class="itemavatar" src="'+defaultAdimg+'" alt="" /></div>')
		}
		return;
	}
	for(x in adArr){
		var tmp = adArr[x];
		if (tmp.length == 0){
			tmp = defaultAdimg;
		}
		$('#adimages').append('<div class="4u 12u"><img id="itemavatar1"class="itemavatar" src="'+tmp.substring(3)+'" alt="" /></div>')
	}
}

function EventShare(){
	FB.ui({
	  method: 'share',
	  href: window.location,		//TO DO Share here
	}, function(response){});
}

function 	DisplayTitle(){
	var title = item.Name;
	if (title.length > 32){
		title = title.substring(0,30)+"...";
	}
	$('#lblItemTitle').text(title);
}

function	DisplayRate(){
	$("#rating").text((item.Vote == 0? "0.0" : item.Vote) +"/5.0");
}

function	DisplayCate(){
	var cate = "";
	var type = item.Type;
	switch (type){
		case "1":
			cate = "CV";
			break;
		case "2":
			cate = "Presentation";
			break;
		case "3":
			cate = "Report";
			break;
	}
	$('#lblCategory').text("Category: "+cate);
}

function	DisplayImage(){
	var path = item.Image.substring(3);
	$("#itemavatar").attr("src",path);
}

function	DisplayDescript(){
	$("#descript").text(item.Descript);
}

function	Download(){
    // // hope the server sets Content-Disposition: attachment!

    window.location = item.File.substring(3);
}

function	DisplayComments(){
	for (i in comments){
		AddComment(comments[i].Content,comments[i].Name);
	}
}

function 	HideLeaveComment(data){
	if( data == '1') {
		$('#leavecomments').hide();
		return;
	}
	$.ajax({
		type : 'POST',
		url :'php/data_get.php',
		data:{
			'function': 'isCommented',
			'id': id 
		},
		success : function(res) {
	
			if (res == 1) {
				$('#leavecomments').hide();
				console.log(000);
			};
		},
	});
}

function 	Comment(){
	var vote = $("#ratebar").val();
	Vote(vote);
	var comment = $("#comment_text").val();
	$.ajax({
		type : 'POST',
		url :'php/data_insert.php',
		data:{
			'function': 'addComment',
			'comment': comment,
			'item': item.ID 
		},
		success : function(res) {
			console.log(res);
			if (res == 1) {
				console.log(res);
				$('#leavecomments').hide('fast');
			};
		}
	});
}

function Vote(vote){
	var curvote = item.Vote;
	var curNumVote = item.NumVote;
	item.NumVote = curNumVote + 1;
	item.Vote = (curvote*curNumVote + vote)/ item.NumVote;
	DisplayRate();
	
	$.ajax({
		type : 'POST',
		url :'php/data_insert.php',
		data:{
			'function': 'addVote',
			'vote': vote,
			'item': id 
		},
		success: function(data) {
			console.log(data);
			
		}
	});
}
function AddComment(content,name){
	console.log(content + name);
	$("#commentlist ul").append($('<li class = "12u$ actions Comment"><div class="row uniform"><div class="12u$ 12u"><h5 id="email_comment">'+name+'</h5></div><div class="12u$ 12u"><p id="content_comment">'+content+'</p></div></div></li>'));
}
