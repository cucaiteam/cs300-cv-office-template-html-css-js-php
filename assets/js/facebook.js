document.getElementById("loginfacebook").addEventListener("click",_login);
// Load the SDK asynchronously
window.fbAsyncInit = function() {
  FB.init({
    appId      : '922245674530253',
    xfbml      : true,
    version    : 'v2.5'
  });
};

(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "assets/js/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
// These three cases are handled in the callback function.
FB.getLoginStatus(function(response) {
  statusChangeCallback(response);
});

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
  if (response.status === 'connected') {
    // Logged into your app and Facebook.
  _i();
  } else if (response.status === 'not_authorized') {
    // The person is logged into Facebook, but not your app.
    document.getElementById('status').innerHTML = 'Please log ' +
      'into this app.';
  }
}

function _login() {
FB.login(function(response) {
   // handle the response
   if(response.status==='connected') {
  _i();
   }
 }, {scope: 'public_profile,email'});
}

function _i(){
 FB.api('/me', function(response) {
  document.getElementById("username").value = response.first_name;
  document.getElementById("name").value = response.last_name;
  document.getElementById("email").value = response.email;
});
}
