var session_email = "";
var session_role = "";
jQuery(function($){
checklogin(navigate);
});
function checklogin(nav){
  $.ajax({
          type: "POST",
          url: 'php/require_login.php',
          data:{
            'function':"getEmail"
          },
          success: nav
  });
}

function navigate(data){
  if(data != '1'){
              startWithUser(data);
            }else{
              finishingChange();
  }
}

function checklogin_handler (data){
      console.log(data);
        if(data == 1){
          nonUser();
        }
        else{
          $.ajax({
            type : 'POST',
            url : 'php/require_login.php',
            data: {
              'function' : "getRole"
            },
            success: function(data) {
                console.log(data);
                session_role = data;
            }
          });
          startWithUser(data);
        }
}

function nonUser(){
  session_email = '0';
  var test = window.location.pathname.split('/');
  console.log(test);
  var testString = test[test.length-1];
  switch (testString) {
    case "account_info.html": case "upload.html":
      window.location.assign("login.html");
      break;
    default:
   }
  finishingChange();
}

function startWithUser(email){
  changeMenu();
  session_email = email;
  var currentPath = window.location.pathname;
  switch (currentPath.substring(currentPath.lastIndexOf("/"))) {
    case "/index.html":
      startIndexWithUser(email);
      break;
    case "/detail.html":
      break;
    case "/aboutus.html":
      break;
    case "/login.html":
      window.location.assign("index.html");
      break;
    case "/signup.html":
      window.location.assign("index.html");
      break;
    case "/account_info.html":
      break;
    case "/upload.html":
      break;
    default:
    break;
  }
  finishingChange();
}

function startIndexWithUser(email){
  var name ='';
  $("#btnsignup").remove();
  $("#btnsignin").remove();
  $.ajax({
    type : 'POST',
    url : 'php/data_get.php',
    data: {
      'function' : "getUserName",
      'email' : email
    },
    success: function(data) {
      if(data != '0'){
        $("#intro").text("Welcome " + data);
      }
    }
  });
}

function logout(){
  $.ajax({
    type: 'POST',
    url : 'php/log_out.php',
    data: {
      'function' : 'logout'
    },
    success: function(data) {
      console.log(data);

      notlogin();
    }
  })
}

function changeMenu(){
  var menuList = $("#menu-list");
  $("#menu-signup").hide();
  $("#menu-signin").hide();
  $('<li id = "menu-logout"><a href="javascript:logout();">Log out</a></li>').insertAfter("#home");
  $('<li id = "menu-upload"><a href="upload.html">Upload</a></li>').insertAfter("#home");
  $('<li id = "menu-account"><a href="account_info.html">Account</a></li>').insertAfter("#menu-upload");
}



function notlogin(){
      $("#menu-signup").show();
      $("#menu-signin").show();
      $('#menu-logout').hide();
      $('#menu-upload').hide();
      $('#menu-account').hide();
      window.location.assign("index.html");
}

function finishingChange(){
  $("#page-wrapper").show();
}
