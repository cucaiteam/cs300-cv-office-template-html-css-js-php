//account_info
$('#btnsEditMode').hide();
/*Declarations*/
getInfo();
//View Variables
var error_card = $('#error_card');

var edName = $('#yourname');
var edMail = $('#youremail');
var edSex = $('#sex');
var edAge = $('#yourage');
var edNationality = $('#yournationality');
var edCardNumber = $('#your_cardnumber');
var edCardSec = $('#your_cardscode');
var radioChosenCard = $('#'+getChosenCard());
var edCardDate = $('#your_carddate');
var edBudget = $('#yourbudget');

//password
var curPass = $('#cur_pass_field');
var divNewPassword = $('#change_pass_fields');
var edCurPass = $('#your_currentpassword');
var edNewPass = $('#your_newpassword');
var edConfPass = $('#conf_newpassword');
var mode = 0;

//Data Variables
var yourName = '';
var yourAge = '';
var yourMail = '';
var yourSex = '';
var yourNationality = '';
var yourCardNumber = '';
var yourCardSec = '';
var yourCardDate = '';
var yourCard = '';
var yourBudget = 0.0;

//Functions async
document.getElementById('btnEdit').addEventListener('click',turnOnEditMode);
document.getElementById('btnPassChange').addEventListener('click',turnOnChangingPassword);
document.getElementById('btnUnsave').addEventListener('click',viewMode);
document.getElementById('btnSave').addEventListener('click',eventSave);
addEventRadioClick();

jQuery(function($){
	checkCardNumber();
	edCardSec.keyup(function(){
		var cardcvv = edCardSec.val();

		if (!checkCVV(cardcvv)){
			error_card.text('invalid cvv');
			error_card.show('fast');
			console.log("$@!$!@");
		}else{
			error_card.text('');
			error_card.hide();
		}
	});

	edCardDate.keyup(function(){
		var carddate = edCardDate.val();
		if (!checkThrudate(carddate)){
			error_card.text('invalid date');
			error_card.show('fast');
		}else{

			error_card.text('');
			error_card.hide();
		}
	});

});





//Functions

function getChosenCard(){
	var radiosGroup = document.getElementsByName('radiosPayChoice');
	for (x in radiosGroup){
		if (radiosGroup[x].checked == true){
			return radiosGroup[x].getAttribute('id');
		}
	}
	return '';
}



function addEventRadioClick(){
	$('#mas').click(handleRadioClick);
	$('#non').click(handleRadioClick);
	$('#vis').click(handleRadioClick);
}

function handleRadioClick(){
	var radio = getChosenCard();
	if(radio == 'non')
	{
		edCardNumber.val('');
		edCardDate.val('');
		edCardSec.val('');
		edCardNumber.prop('disabled',true);
		edCardSec.prop('disabled',true);
		edCardDate.prop('disabled',true);
		error_card.text('');
		error_card.hide();
	}
	else{
		edCardNumber.prop('disabled',false);
		edCardSec.prop('disabled',false);
		edCardDate.prop('disabled',false);	
		error_card.text('Fill up these fields');

		error_card.show();
	}
}

function turnOnEditMode(){
	mode =1;
	edName.prop('disabled',false);
	edSex.prop('disabled',false);
	edAge.prop('disabled',false);
	edNationality.prop('disabled',false);
	edCardNumber.prop('disabled',false);
	edCardSec.prop('disabled',false);
	edCardDate.prop('disabled',false);
	$('#mas').prop('disabled',false);
	$('#vis').prop('disabled',false);
	$('#non').prop('disabled',false);$('#btnsEditMode').show('fast');
	$('#btnsModeView').hide();
	curPass.show();
	divNewPassword.hide();

}

function turnOnChangingPassword(){
	mode = 2
	edName.prop('disabled',true);
	edMail.prop('disabled',true);
	edSex.prop('disabled',true);
	edAge.prop('disabled',true);
	edNationality.prop('disabled',true);
	edCardNumber.prop('disabled',true);
	edCardSec.prop('disabled',true);
	edCardDate.prop('disabled',true);
	$('#mas').prop('disabled',true);
	$('#vis').prop('disabled',true);
	$('#non').prop('disabled',true);
	$('#btnsEditMode').show('fast');
	$('#btnsModeView').hide('fast');
	
	curPass.show();
	divNewPassword.show();
}

function viewMode(){
	mode = 0
	updateUI();
	edName.prop('disabled',true);
	edMail.prop('disabled',true);
	edSex.prop('disabled',true);
	edAge.prop('disabled',true);
	edNationality.prop('disabled',true);
	edCardNumber.prop('disabled',true);
	edCardSec.prop('disabled',true);
	edCardDate.prop('disabled',true);
	$('#mas').prop('disabled',true);
	$('#vis').prop('disabled',true);
	$('#non').prop('disabled',true);
	curPass.hide();
	divNewPassword.hide();
	$('#btnsEditMode').hide('fast');
	$('#btnsModeView').show('fast');
}

function getInfo(){
	$.ajax({
		type : 'POST',
		url : 'php/data_get.php',
		data: {
			'function' : 'getUserInfo'
		},
		success: function(response){
			if(response.length == 0){
				console.log('wrong');
				return;
			}
			var obj = JSON.parse(response);
			//TO DO extract obj
			yourName = obj.Name;
			yourMail = obj.Email;
			yourSex = obj.Sex;
			yourAge = obj.Age;
			yourNationality = obj.Nationality;
			yourCard = obj.CardType;
			yourCardNumber = obj.CardNumber;
			yourCardSec = obj.CVV;
			yourCardDate = obj.CardDate;
			yourBudget = obj.Budget;
			updateUI();
		}
	});
}


function updateUI(){
	edName.val(yourName);
	edMail.val(yourMail);
	edSex.val(yourSex);
	edAge.val(yourAge);
	edNationality.val(yourNationality);
	edCardNumber.val(yourCardNumber);
	edCardSec.val(yourCardSec);
	edBudget.val(yourBudget);
	edCardDate.val(yourCardDate);
	document.getElementById(yourCard).checked = true;
}

function eventSave(){
	if(error_card.val().length > 0 ){
		console.log(error_card.val().length);
		return;	
	} 
	if($('#your_currentpassword').val().length == 0){
		invokeError('Required Field',$('#error_curpassword'));
	}
	var pwd = $('#your_currentpassword').val();

	switch (mode){
		case 1:
		var yourName1 = edName.val();
			yourSex1 = edSex.val();
			yourAge1 = edAge.val();
			yourNationality1 = edNationality.val();
			yourCardNumber1 = edCardNumber.val();
			yourCardSec1 = edCardSec.val();
			yourCardDate1 = edCardDate.val();
			yourCard1 = getChosenCard();

		
		$.ajax({
			type: 'POST',
			url : 'php/data_get.php',
			data: {
				'function' : 'editUser',
				'pwd'	:pwd,
				'Name' : yourName1,
				'Age'	: yourAge1,
				'Sex'	: yourSex1,
				'Nationality': yourNationality1,
				'CardNumber': yourCardNumber1,
				'CardSec': yourCardSec1,
				'CardDate': yourCardDate1,
				'CardType' : yourCard1    	
			},
			success: editCallback
		});
		break;

		case 2 :
			var your_newpassword = $('#your_newpassword').val();
				conf_newpassword = $('#conf_newpassword').val();
			if(!changePasswordChecks(pwd,your_newpassword,conf_newpassword))
				return;
			$.ajax({
				type: 'post',
				url :'php/data_get.php',
				data: {
					'function' : 'changePassword',
					'newpassword' : your_newpassword,
					'oldpassword' : pwd
				},
				success: editCallback
			});

		default:
		break;
	}

}


function updateData(){
	yourName = edName.val();
	yourSex = edSex.val();
	yourAge = edAge.val();
	yourNationality = edNationality.val();
	yourCardNumber = edCardNumber.val();
	yourCardSec = edCardSec.val();
	yourCardDate = edCardDate.val();
	yourCard = getChosenCard();
}

function invokeError(string, view){
	view.text(string);
	view.show();
}

function changePasswordChecks(your_currentpassword,your_newpassword,conf_newpassword){
	if (your_newpassword.length < 6){
		invokeError('Not less than 6 characters', $('#error_newpassword'));
		return false;
	}

	if (your_newpassword == your_currentpassword){
		invokeError('The same as your old password', $('#error_newpassword'));
		return false;
	}

	if (your_newpassword != conf_newpassword){
		invokeError('Different from the above',$('#error_confpassword'));
		return false;
	}
	return true;
}

function hideError(){
	error_card.hide();
	$('#error_general').hide();
	$('#error_curpassword').hide();
	$('#error_confpassword').hide();
	$('#error_newpassword').hide();
}

function editCallback(response){
	if (response == 2) {
		invokeError('Wrong Password',$('#error_curpassword'));
	}
	else
	
	if (response == 0){
		invokeError('Unknown Errors',$('#error_general'));
	}
	else {
		console.log(response);
		invokeError('Successfully Changed',$('#error_general'));
		updateData();
		hideError();	
		viewMode();
	}
}

function checkCVV(cvv){
  // cvv 
   var cvvRE = /^[0-9]{3,4}$/;
  // var nameRE = /^[A-Za-z0-9_-\s]{4,15}$/;
  if(cvvRE.test(cvv)){
    return true;
  }
  else{
    return false;
  }

}

function checkThrudate(thrudate){
	var thrudateRE =  /^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/;
	if(thrudateRE.test(thrudate)){
		return true;
	}
	return false;
}

function checkCardNumber(){
	edCardNumber.validateCreditCard(function(result){
		if(!result.valid && !($('#non').is(':checked'))){
			error_card.text('invalid card number');
			error_card.show('fast');
		}else{

			error_card.text('');
			error_card.hide('fast');
		}
	})
}