/*Initial Work*/
var types = null;
loadItemType();
$('#price').hide();
$('#error_uploadfile').hide();
// Declaration
var progressView = $('#progressView');
progressView.hide();

var progressBar = $('.progress div');
var price = document.getElementById('price');
var itemType = document.getElementById('itemtype');

itemType.onchange = function (){
	if (itemType.value==0) {
		$('#extension').text("");
		return;
	}
	$('#extension').text(types[itemType.value-1].Extension);
}

// Onchange uploadbtn
document.getElementById("uploadBtn").onchange = function () {
	if (itemType.value == 0) {
		document.getElementById("uploadFile").value = this.value = "";
		$('#error_uploadfile').text('Choose Category First!');
		$('#error_uploadfile').show('fast');
		return;
	}
	var extArr = types[itemType.value-1].Extension.split(",");
    	tmp = this.value.split('.');
    	curFileExt = tmp[tmp.length-1];

    if(extArr.indexOf(curFileExt.toLocaleLowerCase()) == -1) {
		document.getElementById("uploadFile").value = this.value = "";
		$('#error_uploadfile').text('This file type is not supported');
    	$('#error_uploadfile').show('fast');
    	return;
    }

    $('#error_uploadfile').hide();
    document.getElementById("uploadFile").value = this.value;
};

//itemavatar click
document.getElementById("itemavatar").addEventListener('click',function(){
	$('#avatarfile').click();
});


//Additional images
document.getElementById("itemavatar1").addEventListener('click',function(){
	$('#adimage1').click();
});

document.getElementById("itemavatar2").addEventListener('click',function(){
	$('#adimage2').click();
});

document.getElementById("itemavatar3").addEventListener('click',function(){
	$('#adimage3').click();
});


//Radio payment
document.ItemForm.radiosPayment[0].addEventListener('click',radiosPaymentChange);
document.ItemForm.radiosPayment[1].addEventListener('click',radiosPaymentChange);



$(document).ready(function (e) {

	//Submitting
	$('#Form').ajaxForm({
        beforeSubmit: function() {
        	if (!checkConstraints()){
        		$('#error_fill').show();
        		return false;
        	}
        	$('#error_fill').hide();
        	progressView.show('fast');
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            progressBar.width(percentVal);
            // percent.html(percentVal);
        },
        complete: function(xhr) {
        	progressView.hide();
            console.log(xhr.responseText);
            alert('Upload Sucessful !');
            window.location.assign("index.html");
        }
    });

	// Function to preview image after validation
	$(function() {
		$("#avatarfile").change(
			{
				error_field: '#error_image',
				imagetarget: '#itemavatar'
			},
			checkImage);
		$('#adimage1').change(
			{
				error_field: '#error_image1',
				imagetarget: '#itemavatar1'
			},
			checkImage);
		$('#adimage2').change(
			{
				error_field: '#error_image2',
				imagetarget: '#itemavatar2'
			},
			checkImage);
		$('#adimage3').change(
			{
				error_field: '#error_image3',
				imagetarget: '#itemavatar3'
			},
			checkImage);
	});

});

/* Handling Functions*/

function checkImage(e) {
			$(e.data.error_field).hide(); // To remove the previous error message
			var file = this.files[0];
			var imagefile = file.type;
			var match = ["image/jpeg","image/png","image/jpg"];
			var error = $(e.data.error_field);

			if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
			{
				error.text('*Invalid image type')
				error.show('fast');
				return false;
			}
			else
			{
				var target = e.data.imagetarget;
				$(e.data.error_field).hide();
				var reader = new FileReader();
				reader.onload = function(e){
					var image = new Image();
					image.src = e.target.result;
					console.log(image.height + " DSA " + image.width);
					if ((Math.abs(image.height - image.width)) > 0.25*image.width) {
						error.text('*Square image only')
						error.show('fast');
						return;
					}
					$(target).attr('src', e.target.result);				
				};	
				reader.readAsDataURL(this.files[0]);
			}		
}

//loading itemtype
function loadItemType(){
	$.ajax({
		type: "POST",
		url: "php/data_get.php",
		data:{
			'function' : 'getItemType'
		},
		success: mapItemTypeToView
	});
}

function mapItemTypeToView(data){
	console.log(data);
	types = JSON.parse(data);
	var html = getTypeSelections(types);
	$('#itemtype').html(html);

}

function radiosPaymentChange(){
	var ele = document.getElementsByName('radiosPayment');
	var i = ele.length;
	// Set the effect type
    var effect = 'slide';
	// Set the duration (default: 400 milliseconds)
    var duration = 500;

	if (ele[0].checked == true){
    	$('#price').hide();
	}
	else{
		$('#price').show();
    }

	console.log("E!$@!");
}

function checkConstraints(){
	var check = $('#title').val().length > 10 ;
	if (check == false){
		$('#error_fill').text('More than 10 characters for the title'); 
		return check;
	}
	check = check && (itemType.value != '0');
	if (check == false){ 
		$('#error_fill').text('Please choose category');
		return check;
	}
	check = check && ($('#descript').val().length > 20);
	if (check == false){ 
		$('#error_fill').text('More than 20 characters for the description');
		return check;
	}
	check = check && ($('#uploadFile').val().length > 4);
	if (check == false){ 
		$('#error_fill').text('No file is chosen');
		return check;
	}
	check = check && ($('#itemavatar').attr('src') != "images/pic04.jpg");
	if (check == false) {
		$('#error_fill').text('Choose demo picture for this');
		return check;
	}
	check = check && ($('#human').is(':checked'));
	if (check == false){
		$('#error_fill').text('Confirm that your are not a robot');
		return check;
	}
	return check;
}


/* HTML Generators */

function getTypeSelections(types){
	var html = "<option value=''>- (*) Category -</option>";
	for (x in types){
		html += "<option value='"+types[x].ID+"'>- "+types[x].TypeName+ "-</option>";
	}
	return html;
}

