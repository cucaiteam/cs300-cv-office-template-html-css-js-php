var itemID = '';
var itemName = '';
var itemDescription = '';
var itemVote = '';
var itemType = '';
var itemUploader = '';
var itemImage = '';

jQuery(function($){
  $.ajax({
    type:'POST',
    url: 'php/data_get.php',
    data:{
      'function':'getItem'
    },
    success:function(data){
      if(data.length == 0){
        return;
      }
      var items = JSON.parse(data);
      
    }
  });
});



function changeData(type, data){
  for (i = 0; i < 5; i++){
    var imageItem = "#" + type + "_item" + i;
    var nameItem = type + "_item" + i + "_name";
    $(imageItem).attr("src", data[i].Image.substring(3));
    document.getElementById(nameItem).innerHTML = data[i].Name;

    var hrefImage = "#href_" + type + i;
    var hrefName = "#href_" + type + "name" + i;
    var hrefLink = "item_details.html?id=" + (new Date(data[i].ID).getTime()/1000 + 25200);
    $(hrefImage).attr("href", hrefLink);
    $(hrefName).attr("href", hrefLink);
  }
}

getFeaturedList();

function getFeaturedList(){
  $.ajax({
    type: 'POST',
    url: 'php/data_get.php',
    data:{
      'function': 'getItemMostVote'
    },
    success:function(response){
      if(response.length == 0){
        return;
      }
      changeData("vote", JSON.parse(response).Result);
    }
  });

  $.ajax({
    type: 'POST',
    url: 'php/data_get.php',
    data:{
      'function': 'getItemMostDownload'
    },
    success:function(response){
      if(response.length == 0){
        return;
      }
      changeData("download", JSON.parse(response).Result);
    }
  });

  $.ajax({
    type: 'POST',
    url: 'php/data_get.php',
    data:{
      'function': 'getItemMostUpload'
    },
    success:function(response){
      if(response.length == 0){
        return;
      }
      changeData("upload", JSON.parse(response).Result);
    }
  });
}