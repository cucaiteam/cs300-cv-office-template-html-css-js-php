<?php
//initial steps
include 'data_config.php';
include 'data_get.php';

if (!isset($_SESSION)){
		session_start();
}

//executions
if (isset($_POST['finishbtn']))
{
	$itemFolderPath = '../images/upload/files/';
	$itemPicFolderPath = '../images/upload/pics/';					
	
	//Generate randomname
	$name = round(microtime(true));
	$itemPath = uploadFile('uploadBtn',$itemFolderPath,$name);
	$picPath = uploadFile('avatarfile',$itemPicFolderPath,$name);
	$adPath = uploadFile('adimage1',$itemPicFolderPath,$name."_1").
				";".uploadFile('adimage2',$itemPicFolderPath,$name."_2").
				";".uploadFile('adimage3',$itemPicFolderPath,$name."_3");
	
	addItem($itemPath,$picPath,$adPath);
}



//Functions
function uploadFile($name_upload,$folderPath,$name){
	if(!isset($_FILES[$name_upload]) || $_FILES[$name_upload]['error'] == UPLOAD_ERR_NO_FILE) {
    	return ""; 
	}
	if(isset($_FILES[$name_upload]["type"]))
	{
		if ($_FILES[$name_upload]["size"] < 50000000)//Approx. 100kb files can be uploaded.
		{
			if ($_FILES[$name_upload]["error"] > 0)
			{
				echo "Return Code: " . $_FILES[$name_upload]["error"] . "<br/><br/>";
			}
			else
			{

				$fileExt = explode(".", $_FILES[$name_upload]["name"]);
				$origiName = $name. '.'.end($fileExt);
				if (file_exists($folderPath.$origiName))
					unlink($folderPath.$origiName);
				
				$sourcePath = $_FILES[$name_upload]['tmp_name']; // Storing source path of the file in a variable
				$targetPath = $folderPath.$origiName; // Target path where file is to be stored
				move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
				echo $targetPath;
				return $targetPath;
			}
		}
		else
		{
			echo "not good";
		}
	}
}

function addItem($item_Path,$pic_Path,$ad_path)
{
	$title = $_POST['title'];
	echo $title;
	$tag = $_POST['tag'];
	echo $tag;
	$description = $_POST['description'];
	echo $description;
	$type = $_POST['itemtype'];
	echo $type;
	$price = $_POST['price'];
	echo $price;
	echo $_SESSION['email'];
	$connection = GetDatabaseConnection();
	

	$sql = "INSERT INTO 
			item (Name, Descript, File, Image, Uploader, Type, Price, Tag, AdImage) 
			VALUES 
			('".$title
				."', '".$description
				."', '".$item_Path
				."', '".$pic_Path
				."', '".$_SESSION['email']
				."', '".$type
				."', '".$price
				."', '".$tag
				."', '".$ad_path
				."')";

	if ($connection -> query($sql) == TRUE) {
	    echo "true";
	} else {
	    echo "false";
	}
	CloseDatabaseConnection();
}


?>