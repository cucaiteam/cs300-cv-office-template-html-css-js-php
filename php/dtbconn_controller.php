<?php function GetDatabaseConnection(){
	global $conn;
	if($conn){
		return $conn;
	}
	$conn = new mysqli($GLOBALS['servername'], $GLOBALS['username'], "", $GLOBALS['dbname']);
	if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
	}
	return $conn;
}

function CloseDatabaseConnection(){
	global $conn;
	if($conn){
		$conn->close();
	}
}
?>
