<?php
include 'data_config.php';
include 'log_in.php';
include 'dtbconn_controller.php';
// Create connection


if(!isset($_SESSION)){
	session_start();
}

$function ='';
if(isset($_POST['function']))
	$function = $_POST['function'];

switch ($function) {
	case 'signIn':
		$email = $_POST['email'];
		$password = $_POST['pwd'];
		@$function($email,$password);
		break;
	case 'isExistUser':
		@$function($_POST['email']);
		break;
	case 'getItemType':
		$function();
		break;
	case 'editUser' :
		@$function();
		break;
	case 'getUserInfo':
	case 'getUserName':
		@$function($_SESSION['email']);
		break;
	case 'changePassword':
		@$function($_POST['oldpassword'],$_POST['newpassword']);
	case 'getItemMostVote':
	case 'getItemMostDownload':
	case 'getItemMostUpload':
		@$function();
		break;
	case 'getItem':
	case 'getComments':
	case 'isCommented':
		@$function($_POST['id']);
		break;
	default:
		break;
}


function getItemMostVote(){
	$connection = GetDatabaseConnection();
	$sql = "SELECT ID, Name, Image FROM item ORDER BY Vote DESC, ID DESC LIMIT 5";
	$result = $connection->query($sql);
	if ($result->num_rows>0){
		$array = array (
			'Result' => array (),
			);
		$i = 0;
		while ($row = mysqli_fetch_array($result))
			$array['Result'][$i++] = $row;
		echo json_encode($array);
	}
	CloseDatabaseConnection();
}
function getItemMostDownload(){
	$connection = GetDatabaseConnection();
	$sql = "SELECT ID, Name, Image FROM item ORDER BY NumDown DESC, ID DESC LIMIT 5";
	$result = $connection->query($sql);
	if ($result->num_rows>0){
		$array = array (
			'Result' => array (),
			);
		$i = 0;
		while ($row = mysqli_fetch_array($result))
			$array['Result'][$i++] = $row;
		echo json_encode($array);
	}
	CloseDatabaseConnection();
}
function getItemMostUpload(){
	$connection = GetDatabaseConnection();
	$sql = "SELECT ID, Name, Image FROM item ORDER BY ID DESC LIMIT 5";
	$result = $connection->query($sql);
	if ($result->num_rows>0){
		$array = array (
			'Result' => array (),
			);
		$i = 0;
		while ($row = mysqli_fetch_array($result))
			$array['Result'][$i++] = $row;
		echo json_encode($array);
	}
	CloseDatabaseConnection();
}


function signIn($email,$pwd){
	$connection = GetDatabaseConnection();
	$sql = "SELECT Password,Role FROM User WHERE Email='".$email."';";
	$result = $connection->query($sql);
	if ($result->num_rows > 0) {
		$user = $result->fetch_assoc();
	    $role = $user["Role"];
	    $pwd_hash = $user["Password"];
			if(hash_equals($pwd_hash,crypt($pwd,$pwd_hash))){
				startSession($email,$role);
				echo 1;
			}
	  	else {
	    	echo 0;
			}
			CloseDatabaseConnection();
	}
	else{
			CloseDatabaseConnection();
			echo 0;
	}
}

function isExistUser($email){
	$connection = GetDatabaseConnection();
	$sql = "SELECT * FROM User WHERE Email='".$email."';";
	$result = $connection->query($sql);
	CloseDatabaseConnection();
	if ($result->num_rows > 0) {
		echo 1;
	}
	else
		echo 0;
}
function getUserName($email){
	$connection = GetDatabaseConnection();
	$sql = "SELECT Name FROM User WHERE Email='".$email."';";
	$result = $connection->query($sql);
	if ($result->num_rows <= 0){
		echo "0";
	}
	$row = $result->fetch_assoc();
	echo $row['Name'];
	CloseDatabaseConnection();
}

function getUserInfo($email){
	$connection = GetDatabaseConnection();

	$sql = "SELECT * FROM User WHERE Email='".$email."';";
	$result = $connection->query($sql);

	if ($result->num_rows < 0){
		echo "";
	}

	$row = $result->fetch_assoc();


	$user = array(
		'Name' => $row['Name'],
		'Email' => $row['Email'],
		'Age' => $row['Age'],
		'Sex' => $row['Sex'],
		'CardNumber' => $row['CardNumber'],
		'CardType' => $row['CardType'],
		'Budget' => $row['Budget'],
		'Nationality' => $row['Nationality'],
		'CVV' => $row['CVV'],
		'CardDate' => $row['CardDate']
		);
	echo json_encode($user);
	CloseDatabaseConnection();
}

function getItemType(){
	$connection = GetDatabaseConnection();
	$sql = "SELECT * FROM itemtype;";
	$result = $connection->query($sql);
	$types = array();
	while($row = $result->fetch_assoc()){
		$type = array(
			'ID' => $row['ID'],
			'TypeName' => $row['TypeName'],
			'Extension' => $row['Extension']
			);
		array_push($types, $type);
	}

	echo json_encode($types);
	CloseDatabaseConnection();
}

function checkPassword($email,$pwd){
	$connection = GetDatabaseConnection();
	$sql = "SELECT Password FROM User WHERE Email='".$email."';";
	$result = $connection->query($sql);
	if ($result->num_rows > 0) {
		$user = $result->fetch_assoc();
	    $pwd_hash = $user["Password"];
		if(hash_equals($pwd_hash,crypt($pwd,$pwd_hash))){
				return true;
		}
	  	else {
	    	return false;
		}
		CloseDatabaseConnection();
	}
	else{

		CloseDatabaseConnection();
		return false;
	}
}

function editUser(){
	$pwd = $_POST['pwd'];
	if (!checkPassword($_SESSION['email'],$pwd)){
		echo 2;
		return;
	}

	$connection = GetDatabaseConnection();


	$Name = $_POST['Name'];
	$Age = $_POST['Age'];
	$Sex = $_POST['Sex'];
	$Nationality = $_POST['Nationality'];
	$CardNumber = $_POST['CardNumber'];
	$CardSec = $_POST['CardSec'];
	$CardDate = $_POST['CardDate'];
	$CardType = $_POST['CardType'];

	$sql = "UPDATE user
	SET Name='".$Name."',
	Age='".$Age."',
	Sex='".$Sex."',
	Nationality='".$Nationality."',
	CardNumber='".$CardNumber."',
	CVV='".$CardSec."',
	CardDate='".$CardDate."',
	CardType='".$CardType."'
	WHERE Email = '".$_SESSION['email']."';";

	$result = $connection->query($sql);
	if ($result === TRUE) {
    	echo 1;
	} else {
    	echo 0;
	}
	CloseDatabaseConnection();
}

function changePassword($old,$new){
	$email = $_SESSION['email'];
	if(!checkPassword($email,$old))
	{
		echo 2;
		return;
	}
	$connection = GetDatabaseConnection();

	$hash_pwd = crypt($new);
	$sql = "UPDATE user SET Password = '".$hash_pwd."'WHERE Email = '".$email."';";
	$result = $connection->query($sql);
	if ($result === TRUE){
		echo 1;
	}else {
		echo 0;
	}
	CloseDatabaseConnection();

}

function getItem($id){
	$tmpstamp = gmdate('Y-m-d H:i:s',$id);
	$connection = GetDatabaseConnection();
	$sql = "SELECT * FROM item WHERE ID = '".$tmpstamp."';";
	$result = $connection->query($sql);
	if ($result->num_rows>0){
		$item = $result->fetch_assoc();
		$itemObj = array(
			'ID'=> strtotime($item['ID']." UTC"),
			'Name' => $item['Name'],
			'Descript'=> $item['Descript'],
			'File'=> $item['File'],
			'Image'=> $item['Image'],
			'Vote'=> $item['Vote'],
			'NumVote'=> $item['NumVote'],
			'NumDown'=> $item['NumDown'],
			'Uploader'=> $item['Uploader'],
			'Published'=> $item['Published'],
			'Type'=> $item['Type'],
			'Price'=> $item['Price'],
			'Tag' => $item['Tag'],
			'AdImage'=> $item['AdImage']
			);
		echo json_encode($itemObj);
	}
	CloseDatabaseConnection();
}

function getComments($item){
	$tmpstamp = gmdate('Y-m-d H:i:s',$item);
	$connection = GetDatabaseConnection();
	$sql = "SELECT Name, Content from user, comments WHERE ItemId='".$tmpstamp."' AND User = Email;";
	$result = $connection->query($sql);
	$comments = array();
	if($result->num_rows>0){
		while($item = $result->fetch_assoc()){
			$tmp = array(
				'Name' => $item['Name'],
				'Content' => $item['Content']
				);
			array_push($comments, $tmp);
		}
		echo json_encode($comments);
	}else{
		echo 0;
	}
	CloseDatabaseConnection();
}

function isCommented($item)
{
	$tmpstamp = gmdate('Y-m-d H:i:s',$item);
	$connection = GetDatabaseConnection();
	$sql = "SELECT * from comments WHERE ItemId ='".$tmpstamp."'AND User='".$_SESSION['email']."';";
	$result = $connection->query($sql);
	if($result->num_rows > 0)
	{
		echo 1;
	}else{
		echo 0;
	}

	CloseDatabaseConnection();
}
// date('Y-m-d h:i:s',$item->timestamp);
//strtotime($timestamp)
?>
