<?php

function startSession($email, $role){
	session_set_cookie_params(0);
	if(!isset($_SESSION)) session_start();
	$_SESSION["login"] = 'true';
	$_SESSION["email"] = $email;
	$_SESSION["role"] = $role;
}
?>
